/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.rockola;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Tefa
 */
public class Main {
    public static void main(String args[]){
        String url = "jdbc:mysql://localhost:3306/rockola";
        String user = "root";
        String password = "123456789";
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection(url,user,password);
            System.out.println("Conexión exitosa "+url);
            String query = "Insert into canciones(id,nombre,genero,año,album,autor) values(10,Take,indi,2019,album1,Cabu)";
            Statement statement = connection.createStatement();
            statement.execute(query);
            
        } catch (ClassNotFoundException ex){
            ex.printStackTrace();
        } catch (SQLException throwables){
            throwables.printStackTrace();
        }
    }
    
}
